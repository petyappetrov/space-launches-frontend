/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'

const Html = ({ content, styleTags, bundleSrc, client }) =>
  <html lang='ru'>
    <head>
      <meta charSet='utf-8' />
      <meta name='viewport' content='width=device-width, initial-scale=1' />
      <title>Launches</title>
      <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700&amp;subset=cyrillic' rel='stylesheet' />
      {styleTags && styleTags}
    </head>
    <body>
      <div id='root' dangerouslySetInnerHTML={{ __html: content }} />
      {client &&
        <script
          charSet='UTF-8'
          dangerouslySetInnerHTML={{
            __html: `window.__APOLLO_STATE__=${JSON.stringify(client.cache.extract())};`
          }}
        />
      }
      {bundleSrc.map((src, i) => <script key={i} src={'/' + src} charSet='UTF-8' />)}
    </body>
  </html>

Html.propTypes = {
  content: PropTypes.string,
  styleTags: PropTypes.array,
  bundleSrc: PropTypes.array.isRequired,
  client: PropTypes.object
}

export default Html
