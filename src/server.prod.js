/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import Koa from 'koa'
import React from 'react'
import { renderToString } from 'react-dom/server'
import { ServerStyleSheet, StyleSheetManager, ThemeProvider } from 'styled-components'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { createHttpLink } from 'apollo-link-http'
import fetch from 'node-fetch'
import { ServerLocation } from '@reach/router'
import { ApolloProvider, renderToStringWithData } from 'react-apollo'
import path from 'path'
import koaStatic from 'koa-static'
import historyApiFallback from 'koa-connect-history-api-fallback'
import { cookies } from 'utils'
import Html from './Html'
import config from './config'
import stats from './stats.json'
import { dark } from './theme'
import App from './App'

function normalizeAssets (assets) {
  if (typeof assets === 'object') {
    return Object.values(assets)
  }
  return Array.isArray(assets) ? assets : [assets]
}

/**
 * Initialize application
 */
const app = new Koa()

app
  .use(koaStatic(path.join(__dirname, '../dist')))
  .use(historyApiFallback({ verbose: false }))
  .use(async (ctx) => {
    const token = cookies.get('_sljwt', ctx.headers.cookie)
    const client = new ApolloClient({
      ssrMode: true,
      link: createHttpLink({
        uri: config.get('graphql'),
        credentials: 'same-origin',
        headers: {
          Authorization: token ? `Bearer ${token}` : ''
        },
        fetch
      }),
      cache: new InMemoryCache()
    })
    const sheet = new ServerStyleSheet()
    const content = await renderToStringWithData(
      <ApolloProvider client={client}>
        <ServerLocation url={ctx.originalUrl}>
          <StyleSheetManager sheet={sheet.instance}>
            <ThemeProvider theme={dark}>
              <App />
            </ThemeProvider>
          </StyleSheetManager>
        </ServerLocation>
      </ApolloProvider>
    )
    const styleTags = sheet.getStyleElement()

    const html = (
      <Html
        content={content}
        client={client}
        styleTags={styleTags}
        bundleSrc={normalizeAssets(stats.assetsByChunkName)
          .filter(path => path.endsWith('.js'))}
      />
    )
    ctx.body = `<!doctype html>\n${renderToString(html)}`
  })

/**
* Starting server
*/
app.listen(config.get('port'), (err) => {
  if (err) {
    throw new Error(err)
  }
  console.log(`Server started on http://localhost:${config.get('port')}`)
})
