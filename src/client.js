/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { render } from 'react-dom'
import { ThemeProvider } from 'styled-components'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { createNetworkStatusNotifier } from 'react-apollo-network-status'
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
import { cookies } from 'utils'
import introspectionQueryResultData from './fragment-types.json'
import App from './App'
import GlobalStyles, { dark } from './ui/theme'
import { ProgressBar } from 'ui/00-atoms'

const httpLink = createHttpLink({
  uri: process.env.GRAPHQL_URI
})

const {
  NetworkStatusNotifier,
  link: networkStatusNotifierLink
} = createNetworkStatusNotifier()

const authLink = setContext((root, { headers }) => {
  const token = cookies.get('_sljwt')
  return {
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : ''
    }
  }
})

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
})

const client = new ApolloClient({
  link: networkStatusNotifierLink.concat(authLink.concat(httpLink)),
  cache: new InMemoryCache({ fragmentMatcher }).restore(window.__APOLLO_STATE__),
  connectToDevTools: true
})

render(
  <ApolloProvider client={client}>
    <ThemeProvider theme={dark}>
      <>
        <GlobalStyles />
        <NetworkStatusNotifier
          render={({ loading }) => <ProgressBar show={loading} />}
        />
        <App />
      </>
    </ThemeProvider>
  </ApolloProvider>,
  document.getElementById('root')
)
