/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { Router } from '@reach/router'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { hot } from 'react-hot-loader'
import { Spinner } from 'ui/00-atoms'
import { Agencies, Agency } from 'features/Agencies'
import { Launches, Launch } from 'features/Launches'
import { Login, Register } from 'features/Auth'

const NotFound = () =>
  <div>Страница не найдена...</div>

export const MeQuery = gql`
  query Me {
    me {
      _id
      name
      email
    }
  }
`

const App = () =>
  <Query query={MeQuery}>
    {({ data, loading }) => {
      if (loading) {
        return <Spinner />
      }
      return (
        <Router>
          <NotFound default />
          <Launches path='/' />
          <Launch path='launches/:id' />
          <Agencies path='agencies' />
          <Agency path='agencies/:id' />
          <Login path='login' me={data.me} />
          <Register path='register' me={data.me} />
        </Router>
      )
    }}
  </Query>

export default hot(module)(App)
