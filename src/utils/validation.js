/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

export const isEmail = (label = 'Некорректный e-mail') =>
  (value) => {
    if (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      return label
    }
  }

export const isRequired = (label = 'Обязательное поле') =>
  (value) => {
    if (!value) {
      return label
    }
  }

export const minLength = (min, label = `Поле должно содержать больше ${min} символов`) =>
  (value) => {
    if (value && value.length < min) {
      return label
    }
  }

export const createValidate = (rules) => (values) => {
  const validated = {}
  for (const key in rules) {
    const errors = rules[key].reduce((prev, rule) => {
      const error = rule(values[key])
      if (error) {
        prev += prev.length ? (', ' + error) : error
      }
      return prev
    }, '')
    if (errors.length) {
      validated[key] = errors
    }
  }
  return validated
}
