/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const isBrowser = typeof window === 'object'

/**
 * @param {string} name
 * @param {Object} [cookies]
 * @return {string}
 */
const get = (name, cookies) => {
  const list = cookies
    ? cookies.split(';')
    : isBrowser ? document.cookie.split(';') : []
  let result = null
  list.find(item => {
    const parts = item.split('=')
    if (parts[0].trim() === name) {
      result = parts[1].trim()
      return true
    }
    return false
  })
  return result
}

/**
* @param {string} name
* @param {string} value
*/
const set = (name, value) => {
  if (document) {
    document.cookie = name + '=' + value + '; path=/'
  }
}

/**
 * @param {string} name
 */
const remove = (name) => {
  if (document) {
    document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;'
  }
}

export default {
  get,
  set,
  remove
}
