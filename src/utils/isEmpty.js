/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

/**
 * @param {object} obj
 */
const isEmpty = (obj) => {
  /* TODO: Add type of array, not only for object  */

  for (const key in obj) {
    return false
  }
  return true
}

export default isEmpty
