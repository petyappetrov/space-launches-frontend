/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Grid, Col, Row } from 'react-styled-flexboxgrid'
import { H1, Button, Spinner } from 'ui/00-atoms'
import { Table, Section } from 'ui/01-molecules'

const transformingOfName = (col) => {
  if (!col) {
    return '–'
  }
  if (col.length > 50) {
    return col.slice(0, 50) + '...'
  }
  return col
}

const transformingOfCountries = (col) => {
  if (!col) {
    return '–'
  }
  const codes = col.split(',')
  if (codes.length > 3) {
    return codes.slice(0, 3).join(', ') + ', ...'
  }
  return codes.join(', ')
}

const transformingOfWiki = (col) => {
  if (!col) {
    return '–'
  }
  return <a href={col} target='_blank' rel='noopener noreferrer'>{col}</a>
}

const AgenciesList = ({ agencies, loadMore, loading }) => {
  if (loading && !agencies) {
    return <Spinner />
  }
  return (
    <Grid>
      <Row>
        <Col xs>
          <H1>Список агенств</H1>
        </Col>
      </Row>
      <Row>
        <Col xs>
          <Section>
            <Table
              data={agencies}
              columns={[
                {
                  header: 'Название',
                  accessor: 'name',
                  transform: transformingOfName
                },
                {
                  header: 'Страна',
                  width: 140,
                  accessor: 'countryCode',
                  transform: transformingOfCountries
                },
                {
                  header: 'Wiki',
                  accessor: 'wikiURL',
                  transform: transformingOfWiki
                }
              ]}
            />
          </Section>
        </Col>
      </Row>
      <Row center='xs'>
        <Col>
          <Button isLoading={loading} onClick={loadMore}>
            Загрузить ещё
          </Button>
        </Col>
      </Row>
    </Grid>
  )
}

AgenciesList.propTypes = {
  agencies: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    type: PropTypes.number.isRequired
  })),
  loadMore: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
}

export default AgenciesList
