/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Header, Footer } from 'ui/02-organisms'
import { AgenciesList } from '../02-organisms'

const AgenciesTemplate = ({ agencies, loading, loadMore }) =>
  <>
    <Header />
    <AgenciesList
      agencies={agencies}
      loading={loading}
      loadMore={loadMore}
    />
    <Footer />
  </>

AgenciesTemplate.propTypes = {
  agencies: PropTypes.array,
  loading: PropTypes.bool.isRequired,
  loadMore: PropTypes.func.isRequired
}

export default AgenciesTemplate
