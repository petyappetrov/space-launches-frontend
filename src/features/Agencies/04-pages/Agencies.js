/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { Query } from 'react-apollo'
import { AgenciesTemplate } from '../03-templates'
import AgenciesQuery from '../05-schemas/AgenciesQuery'

const Agencies = () =>
  <Query
    ssr={false}
    query={AgenciesQuery}
    notifyOnNetworkStatusChange
  >
    {({ data, loading, fetchMore }) =>
      <AgenciesTemplate
        agencies={data.agencies}
        loading={loading}
        loadMore={() =>
          fetchMore({
            variables: {
              offset: data.agencies.length
            },
            updateQuery: (prev, { fetchMoreResult }) => ({
              ...prev,
              agencies: [
                ...prev.agencies,
                ...fetchMoreResult.agencies
              ]
            })
          })
        }
      />
    }
  </Query>

export default Agencies
