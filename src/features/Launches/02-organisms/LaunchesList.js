/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Grid, Col, Row } from 'react-styled-flexboxgrid'
import { Button, Spinner } from 'ui/00-atoms'
import { LaunchItem } from 'ui/01-molecules'

const LaunchesList = ({ launches, loadMore, loading }) => {
  if (loading && !launches) {
    return <Spinner />
  }
  return (
    <Grid>
      <Row>
        {launches.map(launch =>
          <Col key={launch.id} md={4} sm={6} xs={12}>
            <LaunchItem {...launch} />
          </Col>
        )}
      </Row>
      <Row center='xs'>
        <Col>
          <Button isLoading={loading} onClick={loadMore}>
            Загрузить ещё
          </Button>
        </Col>
      </Row>
    </Grid>
  )
}

LaunchesList.propTypes = {
  launches: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    windowstart: PropTypes.string.isRequired
  })),
  loadMore: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
}

export default LaunchesList
