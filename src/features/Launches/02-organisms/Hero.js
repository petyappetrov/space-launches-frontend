/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Spinner } from 'ui/00-atoms'
import { Countdown } from 'ui/01-molecules'

const Wrapper = styled.div`
  background-color: #fff;
`

const Inner = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  flex-direction: column;
  padding: 5rem 0;
`

const Title = styled.div`
  font-size: 36px;
  font-weight: bold;
  margin-bottom: 1rem;
`

const HeroCoundown = styled(Countdown)`
  margin: 2rem;
`

const Hero = ({ launch, loading }) => {
  if (loading && !launch) {
    return <Spinner />
  }
  return (
    <Wrapper>
      <Inner>
        <Title>{launch.name}</Title>
        <HeroCoundown date={launch.windowstart} />
      </Inner>
    </Wrapper>
  )
}

Hero.propTypes = {
  launch: PropTypes.object,
  loading: PropTypes.bool.isRequired
}

export default Hero
