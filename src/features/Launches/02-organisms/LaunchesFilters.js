/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import dayjs from 'dayjs'
import { Select } from 'ui/00-atoms'
import { Grid, Col, Row } from 'react-styled-flexboxgrid'
import styled from 'styled-components'

const LaunchesFiltersStyled = styled.div`
  padding: 2rem;
`

const LaunchesFiltes = ({ variables, setVariable }) =>
  <LaunchesFiltersStyled>
    <Grid>
      <Row between='md' middle='md'>
        <Col>
          <Select
            value={variables.startdate ? 'future' : 'past'}
            onChange={(value) => {
              if (value === 'future') {
                setVariable({
                  enddate: undefined,
                  startdate: dayjs().format('YYYY-MM-DD')
                })
              } else {
                setVariable({
                  enddate: dayjs().format('YYYY-MM-DD'),
                  startdate: undefined
                })
              }
            }}
            options={[
              {
                value: 'future',
                label: 'Предстоящие'
              },
              {
                value: 'past',
                label: 'Запущенные'
              }
            ]}
          />
        </Col>
        <Col>
          <Select
            value={variables.sort}
            onChange={(sort) => setVariable({ sort })}
            label='Сортировать'
            options={[
              {
                value: 'asc',
                label: 'По убыванию'
              },
              {
                value: 'desc',
                label: 'По возвростанию'
              }
            ]}
          />
        </Col>
      </Row>
    </Grid>
  </LaunchesFiltersStyled>

LaunchesFiltes.propTypes = {
  setVariable: PropTypes.func.isRequired,
  variables: PropTypes.object.isRequired
}

export default LaunchesFiltes
