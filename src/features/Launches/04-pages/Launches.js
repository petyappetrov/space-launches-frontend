/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import dayjs from 'dayjs'
import { Query } from 'react-apollo'
import { withStateHandlers } from 'recompose'
import { LaunchesTemplate } from '../03-templates'
import LaunchesQuery from '../05-schemas/LaunchesQuery'

const Launches = ({ variables, setVariable }) =>
  <Query
    ssr={false}
    query={LaunchesQuery}
    notifyOnNetworkStatusChange
    variables={variables}
  >
    {({ data, loading, fetchMore }) => {
      return (
        <LaunchesTemplate
          launches={data.launches}
          loading={loading}
          setVariable={setVariable}
          variables={variables}
          loadMore={() =>
            fetchMore({
              variables: {
                offset: data.launches.length
              },
              updateQuery: (prev, { fetchMoreResult }) => ({
                ...prev,
                launches: [
                  ...prev.launches,
                  ...fetchMoreResult.launches
                ]
              })
            })
          }
        />
      )
    }}
  </Query>

Launches.propTypes = {
  variables: PropTypes.object.isRequired,
  setVariable: PropTypes.func.isRequired
}

const enhance = withStateHandlers(
  {
    variables: {
      startdate: dayjs().format('YYYY-MM-DD'),
      sort: 'asc'
    }
  },
  {
    setVariable: ({ variables }) => (variable) => ({
      variables: {
        ...variables,
        ...variable
      }
    })
  }
)
export default enhance(Launches)
