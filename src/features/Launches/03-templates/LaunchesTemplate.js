/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Header, Footer } from 'ui/02-organisms'
import { LaunchesList, LaunchesFilters, Hero } from '../02-organisms'

const LaunchesTemplate = ({ launches, loading, loadMore, variables, setVariable }) =>
  <>
    <Header />
    <Hero
      loading={loading}
      launch={launches && launches[0]}
    />
    <LaunchesFilters
      variables={variables}
      setVariable={setVariable}
    />
    <LaunchesList
      launches={launches}
      loadMore={loadMore}
      loading={loading}
    />
    <Footer />
  </>

LaunchesTemplate.propTypes = {
  launches: PropTypes.array,
  loading: PropTypes.bool.isRequired,
  loadMore: PropTypes.func.isRequired,
  setVariable: PropTypes.func.isRequired,
  variables: PropTypes.object.isRequired
}

export default LaunchesTemplate
