/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import { graphql, compose, withApollo } from 'react-apollo'
import { withFormik, Form } from 'formik'
import { getGraphQLErrors } from 'utils'
import { createValidate, isRequired, isEmail, minLength } from 'utils/validation'
import { Button } from 'ui/00-atoms'
import { Field } from 'ui/01-molecules'
import RegisterMutation from '../05-schemas/RegisterMutation.gql'

const InputField = styled.div`
  margin-bottom: 1rem;
`

const RegisterForm = () =>
  <Form>
    <InputField>
      <Field.Input
        fullWidth
        label='Почта'
        name='email'
        placeholder='Введите почту'
      />
    </InputField>
    <InputField>
      <Field.Input
        fullWidth
        label='Имя'
        name='name'
        placeholder='Введите имя'
      />
    </InputField>
    <InputField>
      <Field.Input
        fullWidth
        label='Пароль'
        name='password'
        type='password'
        placeholder='Придумайте пароль'
      />
    </InputField>
    <InputField>
      <Button
        fullWidth
        type='submit'
      >
        Регистрация
      </Button>
    </InputField>
  </Form>

const validate = createValidate({
  email: [
    isRequired('Пожалуйста введите email'),
    isEmail('Вы ввели неправильную почту')
  ],
  name: [
    isRequired('Пожалуйста введите имя')
  ],
  password: [
    isRequired('Пожалуйста введите пароль'),
    minLength(6, 'Пароль должен содержать не мене 6 символов')
  ]
})

const enhance = compose(
  withApollo,
  graphql(RegisterMutation, { name: 'register' }),
  withFormik({
    validate,
    mapPropsToValues: () => ({ email: '', password: '', name: '' }),
    handleSubmit: (variables, { props, setErrors }) =>
      props.register({ variables })
        .then(({ data }) => props.history.push({ pathname: '/login' }))
        .catch(({ graphQLErrors = [] }) =>
          graphQLErrors
            .map(getGraphQLErrors)
            .forEach(setErrors))
  })
)

export default enhance(RegisterForm)
