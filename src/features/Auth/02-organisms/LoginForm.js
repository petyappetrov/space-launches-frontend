/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import { graphql, compose, withApollo } from 'react-apollo'
import { withFormik, Form } from 'formik'
import { cookies, getGraphQLErrors } from 'utils'
import { createValidate, isRequired, isEmail, minLength } from 'utils/validation'
import { Button } from 'ui/00-atoms'
import { Field } from 'ui/01-molecules'
import LoginMutation from '../05-schemas/LoginMutation'

const InputField = styled.div`
  margin-bottom: 1rem;
`

const LoginForm = () =>
  <Form>
    <InputField>
      <Field.Input
        fullWidth
        label='Почта'
        name='email'
        placeholder='Введите почту'
      />
    </InputField>
    <InputField>
      <Field.Input
        fullWidth
        label='Пароль'
        name='password'
        type='password'
        placeholder='Введите пароль'
      />
    </InputField>
    <InputField>
      <Button
        fullWidth
        type='submit'
      >
        Вход
      </Button>
    </InputField>
  </Form>

const validate = createValidate({
  email: [
    isRequired('Пожалуйста введите email'),
    isEmail('Вы ввели неправильную почту')
  ],
  password: [
    isRequired('Пожалуйста введите пароль'),
    minLength(6, 'Пароль должен содержать не мене 6 символов')
  ]
})

const enhance = compose(
  withApollo,
  graphql(LoginMutation, { name: 'login' }),
  withFormik({
    validate,
    mapPropsToValues: () => ({ email: '', password: '' }),
    handleSubmit: (variables, { props, setErrors }) =>
      props.login({ variables })
        .then(({ data }) => cookies.set('_sljwt', data.loginUser.jwt))
        .then(props.client.resetStore)
        .catch(({ graphQLErrors = [] }) =>
          graphQLErrors
            .map(getGraphQLErrors)
            .forEach(setErrors))
  })
)

export default enhance(LoginForm)
