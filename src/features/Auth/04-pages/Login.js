/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Link, Redirect } from '@reach/router'
import { Text } from 'ui/00-atoms'
import { AuthTemplate } from '../03-templates'
import { LoginForm } from '../02-organisms'

const LoginHeader = () =>
  <>
    <div>
      Выполнить вход
    </div>
    <div>
      Вы ещё не зарегистрированы? <Link to='/register'>Регистрация</Link>
    </div>
  </>

const LoginFooter = () =>
  <span>
    Войти через Twitter
  </span>

const Login = ({ me }) => {
  if (!me) {
    return <Redirect to='/' />
  }
  return (
    <AuthTemplate
      header={<LoginHeader />}
      footer={<LoginFooter />}
    >
      <LoginForm />
      <Text>
        Забыли пароль? <Link to='/'>Восстановить пароль</Link>
      </Text>
    </AuthTemplate>
  )
}

Login.propTypes = {
  me: PropTypes.object
}

export default Login
