/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { Link } from '@reach/router'
import { Text } from 'ui/00-atoms'
import { AuthTemplate } from '../03-templates'
import { RegisterForm } from '../02-organisms'

const RegisterHeader = () =>
  <>
    <div>
      Регистрация
    </div>
    <div>
      Вы уже зарегистрированы? <Link to='/login'>Вход</Link>
    </div>
  </>

const RegisterFooter = () =>
  <span>
    Зарегистрироваться через Twitter
  </span>

const Register = () =>
  <AuthTemplate
    header={<RegisterHeader />}
    footer={<RegisterFooter />}
  >
    <RegisterForm />
    <Text>
      Нажимая кнопнку &laquo;Регистрация&raquo;,
      вы&nbsp;соглашаетесь с&nbsp;<Link to='/terms'>Условиями&nbsp;использования</Link>
    </Text>
  </AuthTemplate>

export default Register
