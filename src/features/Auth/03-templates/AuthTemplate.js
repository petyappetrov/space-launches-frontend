/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Logo } from 'ui/00-atoms'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  flex-grow: 1;
`

const Background = styled.div`
  position: absolute;
  width: 70%;
  height: 100%;
  transform: skew(40deg);
  overflow: hidden;
  &::before,
  &::after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
  }
  &::before {
    transform: skew(-40deg);
    background: url('https://images.unsplash.com/photo-1517976487492-5750f3195933?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5ab4ea4fd87e2144a8662bee0164010d&auto=format&fit=crop&w=3000&q=80');
    background-position: center;
    left: -100%;
    width: 200%;
    height: 200%;
  }
  &::after {
    background: rgba(12, 52, 75, .9);
  }
`

const FormBlock = styled.div`
  z-index: 1;
  position: relative;
  width: 440px;
  background: #fff;
  border-radius: 8px;
  box-shadow: ${p => p.theme.shadow};
`

const LogoAuth = styled(Logo)`
  position: absolute;
  top: 3rem;
  left: 4rem;
`

const Header = styled.div`
  padding: 3rem 2rem 1rem;
  text-align: center;
  color: ${p => p.theme.colors.black};
  > *:first-child {
    font-weight: 600;
    font-size: 20px;
    margin-bottom: .5rem;
  }
`

const Content = styled.div`
  padding: 0 2rem;
  margin-bottom: 2rem;
  text-align: center;
`

const Footer = styled.div`
  border-top: 2px solid ${p => p.theme.colors.white};
  padding: 2rem 2rem 2rem;
  text-align: center;
`

const FormTemplate = ({ header, children, footer }) =>
  <Wrapper>
    <Background />
    <LogoAuth />
    <FormBlock>
      <Header>
        {header}
      </Header>
      <Content>
        {children}
      </Content>
      <Footer>
        {footer}
      </Footer>
    </FormBlock>
  </Wrapper>

FormTemplate.propTypes = {
  header: PropTypes.element.isRequired,
  footer: PropTypes.element.isRequired,
  children: PropTypes.node.isRequired
}

export default FormTemplate
