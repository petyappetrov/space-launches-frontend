/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { Link } from '@reach/router'
import styled from 'styled-components'
import { Col, Row } from 'react-styled-flexboxgrid'
import { Query } from 'react-apollo'
import { MeQuery } from 'App'
import { Button } from 'ui/00-atoms'

const MenuLink = styled((props) =>
  <Link
    {...props}
    getProps={(p) => ({
      style: {
        fontWeight: p.isCurrent ? 'bold' : 'normal'
      }
    })}
  />
)`
  font-size: 16px;
  text-decoration: none;
  color: #fff;
  margin: 0 1rem;
  &:first-child {
    margin-left: 0;
  }
  &:last-child {
    margin-right: 0;
  }
  &:hover {
    text-decoration: none;
  }
`

const Menu = () =>
  <Query query={MeQuery} fetchPolicy='cache-only'>
    {({ data }) => {
      return (
        <Row end='xs'>
          <Col>
            <MenuLink to='/'>Запуски</MenuLink>
            <MenuLink to='/agencies'>Агентства</MenuLink>
            <MenuLink to='/calendar'>Календарь</MenuLink>
            {data.me
              ? <MenuLink to='/profile'>Профиль</MenuLink>
              : <Button ml='2rem' view='primary' to='/login'>Войти</Button>
            }
          </Col>
        </Row>
      )
    }}
  </Query>

export default Menu
