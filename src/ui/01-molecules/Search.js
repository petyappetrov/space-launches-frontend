/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Input, Button, Logo } from 'ui/00-atoms'
import { Col, Row } from 'react-styled-flexboxgrid'
import { compose, withStateHandlers } from 'recompose'

import styled from 'styled-components'

const SearchLogo = styled(Logo)`
  margin-right: 2rem;
`

const SearchInput = styled(Input)`
  width: 200px;
`

const Search = ({ value, onChangeValue }) =>
  <Row middle='xs'>
    <Col md={4}>
      <SearchLogo />
    </Col>
    <Col md={8}>
      <SearchInput
        name='search'
        value={value}
        onChange={event => onChangeValue(event.target.value)}
        placeholder='Быстрый поиск'
      />
      <Button
        view='primary'
        onClick={() => {}}
      >
        Поиск
      </Button>
    </Col>
  </Row>

Search.propTypes = {
  value: PropTypes.string.isRequired,
  onChangeValue: PropTypes.func.isRequired
}

const enhance = compose(
  withStateHandlers(
    {
      value: ''
    },
    {
      onChangeValue: () => (value) => ({ value })
    }
  )
)

export default enhance(Search)
