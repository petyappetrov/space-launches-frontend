/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const TableWrapper = styled.table`
  width: 100%;
`

const TableHeader = styled.thead`
  text-align: left;
`

const TableHeaderColumn = styled.th`
  padding-bottom: 0px;
  vertial-align: bottom;
`

const TableBodyColumn = styled.td`
  padding: 15px 10px 0 0;
  &:last-child {
    padding-right: 0;
  }
`

const Table = ({ data, columns, keyField = 'id' }) =>
  <TableWrapper>
    <TableHeader>
      <tr>
        {columns.map((column, i) =>
          <TableHeaderColumn
            key={i}
            width={column.width || 'auto'}
          >
            {column.header}
          </TableHeaderColumn>
        )}
      </tr>
    </TableHeader>
    <tbody>
      {data.map((row) =>
        <tr key={row[keyField]}>
          {columns.map((col, i) =>
            <TableBodyColumn key={i}>
              {typeof col.transform === 'function'
                ? col.transform(row[col.accessor], row)
                : row[col.accessor]
              }
            </TableBodyColumn>
          )}
        </tr>
      )}
    </tbody>
  </TableWrapper>

Table.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  keyField: PropTypes.string
}

export default Table
