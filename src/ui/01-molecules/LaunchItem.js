/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import dayjs from 'dayjs'

const LaunchStyled = styled.div`
  width: 100%;
  height: 150px;
  background-color: #fff;
  margin-bottom: 1rem;
  border-radius: 6px;
  box-shadow: ${p => p.theme.shadow};
  padding: 2rem;
  box-sizing: border-box;
`

const Name = styled.div`
  font-size: 18px;
  font-weight: bold;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`

const Date = styled.div`
  margin-top: .5rem;
`

const LaunchItem = ({ name, windowstart }) =>
  <LaunchStyled>
    <Name>
      {name}
    </Name>
    <Date>
      {dayjs(windowstart).format('H:mm / MMM D / YYYY')}
    </Date>
  </LaunchStyled>

LaunchItem.propTypes = {
  name: PropTypes.string.isRequired,
  windowstart: PropTypes.string.isRequired
}

export default LaunchItem
