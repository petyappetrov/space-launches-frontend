/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'formik'
import { Input, Text, Label } from 'ui/00-atoms'

Field.Input = ({ name, label, ...props }) =>
  <Field
    name={name}
    render={({
      field,
      form: {
        touched,
        errors
      }
    }) =>
      <>
        {
          label &&
            <Label
              htmlFor={name}
              error={touched[name] && errors[name]}
            >
              {label}
            </Label>
        }
        <Input
          name={name}
          error={touched[name] && errors[name]}
          {...field}
          {...props}
        />
        {
          touched[name] && errors[name] &&
            <Text error>{touched[name] && errors[name]}</Text>
        }
      </>
    }
  />

Field.Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string
}

export default Field
