/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import dayjs from 'dayjs'

const CountdownStyled = styled.div`
  display: flex;
  justify-content: flex-start;
`

const Col = styled.div`
  width: 100px;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin: 0 7px;
`

const ColValue = styled.div`
  font-size: 46px;
`

const ColLabel = styled.div`
  font-size: 14px;
`

class Countdown extends React.Component {
  static propTypes = {
    date: PropTypes.string.isRequired
  }

  componentDidMount () {
    if (this.getDiff() > 0) {
      this.startTimer()
    }
  }

  componentWillUnmount () {
    if (this.interval) {
      clearInterval(this.interval)
    }
  }

  startTimer = () => {
    this.interval = setInterval(() => this.tick(), 1000)
  }

  tick = () => {
    this.forceUpdate()
  }

  getDiff = () => {
    return dayjs(this.props.date).diff(dayjs(), 'seconds')
  }

  getDelta = (seconds) => {
    return {
      days: Math.floor(seconds / (3600 * 24)),
      hours: Math.floor((seconds / 3600) % 24),
      minutes: Math.floor((seconds / 60) % 60),
      seconds: Math.floor(seconds % 60)
    }
  }

  render () {
    const diff = this.getDiff()
    if (diff < 0) {
      return null
    }
    const { days, hours, minutes, seconds, className = '' } = this.getDelta(diff)
    return (
      <CountdownStyled className={className}>
        <Col>
          <ColValue>{days}</ColValue>
          <ColLabel>дней</ColLabel>
        </Col>
        <Col>
          <ColValue>{hours}</ColValue>
          <ColLabel>часов</ColLabel>
        </Col>
        <Col>
          <ColValue>{minutes}</ColValue>
          <ColLabel>минут</ColLabel>
        </Col>
        <Col>
          <ColValue>{seconds}</ColValue>
          <ColLabel>секунд</ColLabel>
        </Col>
      </CountdownStyled>
    )
  }
}

export default Countdown
