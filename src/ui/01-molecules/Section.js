/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Section = styled.div`
  padding: 2rem;
  box-sizing: border-box;
  background-color: #fff;
  width: 100%;
  margin-bottom: 1rem;
  border-radius: 6px;
  box-shadow: ${p => p.theme.shadow};
`

export default Section
