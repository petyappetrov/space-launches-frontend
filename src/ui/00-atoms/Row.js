/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Row = styled.div`
  margin: 0 -1rem;
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`

export default Row
