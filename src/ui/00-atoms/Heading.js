/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

export const H1 = styled.h1`
  margin: 3rem 0;
  font-size: 32px;
`

export const H2 = styled.h2`
  margin: 2rem 0;
  font-size: 26px;
`

export const H3 = styled.h3`
  margin: 2rem 0;
  font-size: 22px;
`
