/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { navigate } from '@reach/router'
import { Spinner } from 'ui/00-atoms'

const views = {
  default: `
    background-color: rgb(12, 52, 75);
    color: #fff;
    &:hover {
      background-color: rgb(24, 65, 88);
    }
  `,
  primary: `
    color: rgb(204, 217, 223);
    background: none;
    transition: color 150ms ease 0s, border 150ms ease 0s,
    color: rgb(239, 243, 245);
    line-height: 36px;
    border-width: 2px;
    border-style: solid;
    border-color: rgb(61, 88, 102);
    &:hover {
      border-color: rgb(143, 166, 178);
    }
  `,
  danger: `
    background-color: #CD0F21;
    color: #fff;
    &:hover {
      background-color: #e43848;
    }
  `
}

const buttonCSS = css`
  text-transform: uppercase;
  white-space: nowrap;
  word-break: keep-all;
  display: inline-block;
  height: 40px;
  line-height: 40px;
  font-size: 14px;
  border-radius: 6px;
  box-sizing: border-box;
  border: none;
  cursor: pointer;
  padding: 0 20px;
  user-select: none;
  font-weight: bold;
  margin: 0 .5rem;
  outline: none;
  text-decoration: none;
  width: ${p => p.fullWidth ? '100%' : 'auto'};
  &:first-child {
    margin-left: 0;
  }
  &:last-child {
    margin-right: 0;
  }
  &:disabled {
    opacity: 0.55;
    pointer-events: none;
    cursor: default;
  }
  ${(p) => views[p.view]}
`

const ButtonStyled = styled.button`
  ${buttonCSS}
  ${p => p.to && `
    &:hover {
      text-decoration: none;
    }
  `}
`

const Icon = styled.span`
  margin-right: 10px;
  margin-left: -5px;
`

class Button extends React.Component {
  static propTypes = {
    /**
     * On click function
     */
    onClick: PropTypes.func,
    /**
     * For the link URL
     */
    to: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ]),
    /**
     * Other views
     */
    view: PropTypes.oneOf([
      'default',
      'primary',
      'danger'
    ]),
    /**
     * HTML type tag
     */
    type: PropTypes.string,
    /**
     * HTML disable tag
     */
    disabled: PropTypes.bool,
    /**
     * Show indicator on loading
     */
    isLoading: PropTypes.bool,
    /**
     * Place for label button
     */
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    /**
     * Icon on the left side
     */
    icon: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ])
  }

  static defaultProps = {
    view: 'default',
    type: 'button',
    isLoading: false,
    disabled: false
  }

  handleClick = (event) => {
    if (this.props.onClick) {
      this.props.onClick(event)
    }
    if (this.props.to) {
      event.preventDefault()
      navigate(this.props.to)
    }
  }

  render () {
    const { children, icon, isLoading, disabled, ...props } = this.props
    return (
      <ButtonStyled
        {...props}
        disabled={isLoading || disabled}
        onClick={this.handleClick}
      >
        {isLoading
          ? <Spinner />
          : (
            <>
              {icon && <Icon>{icon}</Icon>}
              {children}
            </>
          )
        }
      </ButtonStyled>
    )
  }
}

export default Button
