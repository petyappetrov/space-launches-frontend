/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Title = styled.div`
  line-height: 1.618em;
  ${p => p.error && 'color: #CD0F21'}
  ${p => p.color ? `color: ${p.color}` : ''};
`

export default Title
