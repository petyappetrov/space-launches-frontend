/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'

const STATE_START = 'STATE_START'
const STATE_FINISH = 'STATE_FINISH'
const STATE_HIDE = 'STATE_HIDE'

const Bar = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 2px;
  z-index: 11;
`

const Progress = styled.div`
  width: 100%;
  height: 2px;
  background-color: rgb(21, 189, 118);
  transition-property: transform;
  transition-duration: 12s;
  transition-timing-function: cubic-bezier(0.04, 0.9, 0.11, 0.9);
  transform: translateX(-100%);
  ${p => {
    switch (p.toggler) {
      case STATE_START: {
        return `
          transform: translateX(0%);
        `
      }
      case STATE_FINISH: {
        return `
          transform: scaleX(100) translateX(0%);
        `
      }
      case STATE_HIDE: {
        return `
          opacity: 0;
          transition: none;
        `
      }
      default: {
        return ``
      }
    }
  }}
`

class ProgressBar extends React.Component {
  state = {
    toggler: STATE_HIDE
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    if (
      nextProps.show === true &&
      prevState.toggler === STATE_HIDE
    ) {
      return {
        toggler: STATE_START
      }
    } else if (
      nextProps.show === false &&
      prevState.toggler === STATE_START
    ) {
      return {
        toggler: STATE_FINISH
      }
    } else {
      return null
    }
  }

  componentDidUpdate (prevProps, prevState) {
    if (
      prevState.toggler === STATE_START &&
      this.state.toggler === STATE_FINISH
    ) {
      setTimeout(() => this.setState({ toggler: STATE_HIDE }), 300)
    }
  }

  render () {
    return (
      <Bar>
        <Progress toggler={this.state.toggler} />
      </Bar>
    )
  }
}

export default ProgressBar
