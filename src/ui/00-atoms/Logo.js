/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import { Link } from '@reach/router'

const LogoLink = styled(Link)`
  font-size: 22px;
  font-weight: bold !important;
  color: ${p => p.theme.colors.white};
  position: relative;
  z-index: 1;
  text-decoration: none;
  &:hover {
    text-decoration: none;
  }
`

const Logo = (props) =>
  <LogoLink to='/' {...props}>
    Space launches
  </LogoLink>

export default Logo
