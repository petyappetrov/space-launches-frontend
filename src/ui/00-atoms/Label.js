/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Label = styled.label`
  display: block;
  margin-bottom: 8px;
  font-weight: 600;
  text-align: left;
  font-size: 14px;
  line-height: 18px;
  color: rgb(12, 52, 75);
  ${(p) => p.error && 'color: #CD0F21;'}
`

export default Label
