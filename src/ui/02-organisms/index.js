/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

export { default as Header } from './Header'
export { default as Footer } from './Footer'
