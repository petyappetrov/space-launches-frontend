/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import { Grid, Col, Row } from 'react-styled-flexboxgrid'
import { Search, Menu } from 'ui/01-molecules'

const HeaderStyled = styled.div`
  padding-top: 20px;
  padding-bottom: 20px;
  background: #141E30;
  background: linear-gradient(to right, #243B55, #141E30);
`

const Header = () =>
  <HeaderStyled>
    <Grid>
      <Row middle='xs'>
        <Col md={7}>
          <Search />
        </Col>
        <Col md={5}>
          <Menu />
        </Col>
      </Row>
    </Grid>
  </HeaderStyled>

export default Header
