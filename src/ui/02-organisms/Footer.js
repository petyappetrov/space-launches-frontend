/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import { Link } from '@reach/router'
import { Grid, Col, Row } from 'react-styled-flexboxgrid'
import { Query } from 'react-apollo'
import { MeQuery } from 'App'
import { Logo } from 'ui/00-atoms'

const FooterStyled = styled.div`
  margin-top: 2rem;
  padding: 45px 0 55px;
  background-color: #122331;
  color: #fff;
  font-size: 14px;
`

const FooterLink = styled(Link)`
  color: rgb(143, 166, 178);
  display: block;
  text-decoration: none;
  margin-bottom: .5rem;
  &:hover {
    color: #fff;
  }
`

const FooterTitle = styled.div`
  text-transform: uppercase;
  font-weight: bold;
  margin-bottom: 10px;
`

const Footer = () =>
  <FooterStyled>
    <Grid>
      <Row>
        <Col md={4} sm={6} xs={12}>
          <FooterTitle>Ресурсы</FooterTitle>
          <FooterLink to='/contacts'>Контакты</FooterLink>
          <FooterLink to='/'>Запуски</FooterLink>
          <FooterLink to='/agencies'>Агенства</FooterLink>
          <FooterLink to='/calendar'>Календарь</FooterLink>
        </Col>
        <Col md={4} sm={6} xs={12}>
          <FooterTitle>Аккаунт</FooterTitle>
          <Query query={MeQuery} fetchPolicy='cache-only'>
            {({ data }) =>
              data.me
                ? (
                  <FooterLink to='/profile'>Профиль</FooterLink>
                )
                : [
                  <FooterLink to='/login' key='login'>Войти</FooterLink>,
                  <FooterLink to='/register' key='register'>Регистрация</FooterLink>
                ]
            }
          </Query>
          <FooterLink to='/feedback'>Обратная связь</FooterLink>
          <FooterLink to='/privacy'>Политика конфиденциальности</FooterLink>
        </Col>
        <Col md={4} sm={12} xs={12}>
          <Logo />
        </Col>
      </Row>
    </Grid>
  </FooterStyled>

export default Footer
