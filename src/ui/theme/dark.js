/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

export default {
  shadow: 'rgba(8, 35, 51, 0.05) 0px 4px 8px',
  colors: {
    black: 'rgb(8, 35, 51)',
    white: 'rgb(239,243,245)',
    green: 'rgb(21, 189, 118)'
  },
  flexboxgrid: {
    gridSize: 12,
    gutterWidth: 1,
    outerMargin: 2,
    mediaQuery: 'only screen',
    container: {
      sm: 46,
      md: 61,
      lg: 76
    },
    breakpoints: {
      xs: 0,
      sm: 48,
      md: 64,
      lg: 75
    }
  }
}
