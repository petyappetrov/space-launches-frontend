/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { createGlobalStyle } from 'styled-components'
import styledNormalize from 'styled-normalize'

export default createGlobalStyle`
  ${styledNormalize};
  #root {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
    background-color: rgb(239,243,245);
    color: rgb(61, 85, 107);
    font-size: 14px;
  }
  *,
  button,
  input,
  optgroup,
  select,
  textarea {
    font-family: 'Montserrat', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  input[type='text'],
  input[type='password'],
  textarea,
  button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    &:focus {
      border-color: #000;
      box-shadow: none;
      outline: none;
    }
  }
  a {
    color: rgb(21, 189, 118);
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
`

export { default as dark } from './dark'
