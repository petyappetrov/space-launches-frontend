/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

require('dotenv').config()

const convict = require('convict')

const config = convict({
  env: {
    env: 'NODE_ENV',
    doc: 'The application environment.',
    format: [
      'production',
      'development'
    ],
    default: 'development'
  },
  port: {
    env: 'PORT',
    doc: 'The port to bind.',
    format: 'port',
    default: 3333
  },
  graphql: {
    env: 'GRAPHQL_URI',
    doc: 'The GraphQL server URL',
    format: 'url',
    default: 'http://localhost:8080/graphql'
  }
})

config.validate({ allowed: 'strict' })

module.exports = config
