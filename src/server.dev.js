/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import Koa from 'koa'
import React from 'react'
import { renderToString } from 'react-dom/server'
import webpack from 'webpack'
import koaWebpack from 'koa-webpack'
import path from 'path'
import koaStatic from 'koa-static'
import historyApiFallback from 'koa-connect-history-api-fallback'
import Html from './Html'
import config from './config'
import webpackConfig from '../webpack/config.dev'

function normalizeAssets (assets) {
  if (typeof assets === 'object') {
    return Object.values(assets)
  }
  return Array.isArray(assets) ? assets : [assets]
}

/**
 * Initialize application
 */
const app = new Koa()

app
  .use(koaStatic(path.join(__dirname, '../dist')))
  .use(historyApiFallback({ verbose: false }))

/**
 * Compile webpack
 */
koaWebpack({
  compiler: webpack(webpackConfig),
  devMiddleware: {
    serverSideRender: true
  }
})
  .then(middleware => {
    app
      .use(middleware)
      .use((ctx) => {
        const stats = ctx.state.webpackStats.toJson()
        const html = (
          <Html
            bundleSrc={normalizeAssets(stats.assetsByChunkName.main)
              .filter(path => path.endsWith('.js'))}
          />
        )
        ctx.body = `<!doctype html>\n${renderToString(html)}`
      })

    app.listen(config.get('port'), (err) => {
      if (err) {
        throw new Error(err)
      }
      console.log(`Server started on http://localhost:${config.get('port')}`)
    })
  })
