/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider } from 'styled-components'
import { dark } from '../theme'

const Template = ({ children }) =>
  <ThemeProvider theme={dark}>
    {children}
  </ThemeProvider>

Template.propTypes = {
  children: PropTypes.node
}

export default Template
