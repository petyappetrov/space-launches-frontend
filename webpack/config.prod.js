/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

require('dotenv').config()

const webpack = require('webpack')
const merge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const baseConfig = require('./config.base')

const config = merge(baseConfig, {
  mode: 'production',
  entry: {
    app: './src/client.js',
    react: [
      'react',
      'react-dom',
      '@reach/router'
    ],
    vendors: [
      'styled-components',
      'stylis',
      'react-apollo',
      'apollo-client'
    ]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader'
        })
      }
    ]
  },
  optimization: {
    splitChunks: {
      automaticNameDelimiter: '-',
      chunks: 'all'
    }
  },
  plugins: [
    // new BundleAnalyzerPlugin(),
    new ExtractTextPlugin('css/[name].[chunkhash].css'),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    })
  ]
})

module.exports = config
